import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app";
import * as serviceWorker from "./serviceWorker";
import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import authReducer from "./store/reducers/auth";
import userFeedReducer from "./store/reducers/userFeed";
import userReducer from "./store/reducers/user";
import locationReducer from "./store/reducers/currentLocation";
const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  auth: authReducer,
  userFeed: userFeedReducer,
  user: userReducer,
  currentLocation: locationReducer
});

const store = createStore(rootReducer, composeEnhances(applyMiddleware(thunk)));
const app = (
  <Provider store={store}>
    <App />
  </Provider>
);
ReactDOM.render(app, document.getElementById("root"));

serviceWorker.unregister();
