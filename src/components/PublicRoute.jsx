import React from "react";
import { Redirect, Route } from "react-router-dom";

const PublicRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated === false ? (
        <Component {...rest} isAuthenticated={isAuthenticated} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

export default PublicRoute;
