import React, { Component } from "react";

import "antd/dist/antd.css";
import { connect } from "react-redux";
import * as actions from "../../store/actions/auth";

import BaseRouter from "../routes";
import { Router } from "react-router";
import createBrowserHistory from "history/createBrowserHistory";
//import { BrowserRouter as Router } from "react-router-dom";

export const history = createBrowserHistory();
class App extends Component {
  componentDidMount() {
    document.body.style.backgroundColor = "#e6ecf0";
    this.props.onTryAutoSignup();
  }

  render() {
    return (
      <Router history={history}>
        <BaseRouter {...this.props} />
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: localStorage.getItem("token") !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
