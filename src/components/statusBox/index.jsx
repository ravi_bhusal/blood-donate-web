import React from "react";

import { Input, Button, Select, Icon, Form } from "antd";

import defaultAvatar from "./defaultAvatar.png";

import { InputNumber } from "antd";

import * as actions from "../../store/actions/user";

import { connect } from "react-redux";

import moment from "moment";

const { TextArea } = Input;

const Option = Select.Option;

class StatusBox extends React.Component {
  state = {
    bloodType: ""
  };

  componentDidMount() {
    Icon.backgroundColor = "green";
  }

  pintsOnChange(value) {
    console.log("changed", value);
  }

  handleTypeChange = value => {
    this.setState({ bloodType: value });
  };

  handleFormSubmit = event => {
    event.preventDefault();
    const postObj = {
      user: this.props.userId,
      required_bloodtype: this.state.bloodType,
      number_of_pints_required: event.target.elements.pints.value,
      status_photo: null,
      extra_info: event.target.elements.extraInfo.value,
      latitude: null,
      longitude: null,
      posted_time: moment(new Date().getTime()).format()
    };

    this.props.setInNeed(this.props.token, postObj);

    console.log(this.props.token);
  };

  render() {
    return (
      <div
        style={{
          height: "150px",
          width: "auto",
          backgroundColor: "#fff"
        }}
      >
        <div
          style={{
            float: "left",
            height: "inherit",
            width: "600px",
            marginRight: "20px",
            marginTop: "10px"
          }}
        >
          <img
            src={defaultAvatar}
            alt="hello"
            width="32px"
            height="32px"
            style={{
              marginLeft: "20px",
              marginTop: "10px",
              position: "absolute"
            }}
          />

          <Form
            onSubmit={this.handleFormSubmit}
            style={{
              marginLeft: "60px",
              marginTop: "10px",
              paddingRight: "5px"
            }}
          >
            <div>
              <TextArea
                name="extraInfo"
                rows={3}
                style={{ border: "2px solid #bb0a1e", resize: "none" }}
              />
            </div>
            <div>
              <Button
                name="pictureButton"
                icon="picture"
                style={{
                  marginLeft: "5px",
                  color: "F08080",
                  backgroundColor: "#fff",
                  textAlign: "center"
                }}
              />
              <p style={{ marginLeft: "70px", display: "inline" }}> Pints</p>
              <InputNumber
                name="pints"
                min={1}
                max={10}
                defaultValue={3}
                onChange={this.pintsOnChange}
                style={{ width: 60, marginLeft: "10px" }}
              />
              <p style={{ marginLeft: "50px", display: "inline" }}> Type</p>
              <Select
                ref="bloodGroup"
                name="bloodGroup"
                defaultValue="-----"
                style={{ width: 60, marginLeft: "10px" }}
                onChange={this.handleTypeChange}
              >
                <Option value="">------</Option>
                <Option value="a+">A+</Option>
                <Option value="a-">A-</Option>
                <Option value="b+">B+</Option>
                <Option value="b-">B-</Option>
                <Option value="ab+">AB+</Option>
                <Option value="ab-">AB-</Option>
                <Option value="o+">O+</Option>
                <Option value="o-">O-</Option>
              </Select>
              <Button
                htmlType="submit"
                type="danger"
                style={{
                  float: "right",
                  marginTop: "1px",
                  backgroundColor: "red",
                  color: "white"
                }}
              >
                Post
              </Button>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setInNeed: (token, postObj) => dispatch(actions.setInNeed(token, postObj))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusBox);
