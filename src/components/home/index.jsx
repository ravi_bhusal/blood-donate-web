import React from "react";
import { Row, Col } from "antd";
import ProfileBar from "../profileBar";
import StatusBox from "../statusBox";
import Feed from "../feed";
import ProfileBox from "../profileBox";
import Location from "../location";
import { connect } from "react-redux";

class HomePage extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Location />
        <ProfileBar {...this.props} style={{ position: "fixed" }} />

        <Row>
          <Col span={2} />
          <Col span={5} style={{ marginTop: "10px" }}>
            <ProfileBox {...this.props} />
          </Col>
          <Col span={10} style={{ marginTop: "10px" }}>
            <StatusBox {...this.props} />
            <Feed {...this.props} />
          </Col>
          <Col span={5} />
          <Col span={2} />
        </Row>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};
export default connect(
  mapStateToProps,
  null
)(HomePage);
