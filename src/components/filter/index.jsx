import React from "react";

import { Select, Form } from "antd";

const Option = Select.Option;

class Filter extends React.Component {
  state = {
    bloodType: "a+",
    statusType: "Available",
    radius: "30km"
  };

  handleFilterChange = (statusType, bloodType, radius) => {
    console.log(statusType, bloodType, radius);
    this.props.onChange(statusType, bloodType, radius);
  };

  handleTypeChange = value => {
    this.setState({ statusType: value });
    this.handleFilterChange(value, this.state.bloodType, this.state.radius);
  };

  handleBloodTypeChange = value => {
    this.setState({ bloodType: value });
    this.handleFilterChange(this.state.statusType, value, this.state.radius);
  };
  handleRadiusChange = value => {
    this.setState({ radius: value });
    this.handleFilterChange(this.state.statusType, this.state.bloodType, value);
  };
  render() {
    return (
      <Form
        style={{ display: "inline-block", marginLeft: "20px" }}
        onChange={this.handleFilterChange.bind(this)}
      >
        <Form.Item style={{ float: "left" }}>
          <h3 style={{ float: "left" }}>Show:</h3>
          <Select
            defaultValue="available"
            style={{ width: 120, marginLeft: "5px", marginBottom: "4px" }}
            onChange={this.handleTypeChange}
          >
            <Option value="available">Available</Option>
            <Option value="inNeed">In Need</Option>
          </Select>
        </Form.Item>

        <Form.Item style={{ float: "left", marginLeft: "50px" }}>
          <h3 style={{ float: "left" }}>Type:</h3>
          <Select
            defaultValue=""
            style={{ width: 70, marginLeft: "5px", marginBottom: "4px" }}
            onChange={this.handleBloodTypeChange}
          >
            <Option value="">-----</Option>
            <Option value="a+">A+</Option>
            <Option value="a-">A-</Option>
            <Option value="b+">B+</Option>
            <Option value="b-">B-</Option>
            <Option value="ab+">AB+</Option>
            <Option value="ab-">AB-</Option>
            <Option value="o+">O+</Option>
            <Option value="o-">O-</Option>
          </Select>
        </Form.Item>
        <Form.Item style={{ float: "left", marginLeft: "50px" }}>
          <h3 style={{ float: "left" }}>Radius:</h3>
          <Select
            defaultValue="30km"
            style={{ width: 100, marginLeft: "5px", marginBottom: "4px" }}
            onChange={this.handleRadiusChange}
          >
            <Option value="30km">30 Km</Option>
            <Option value="20km">20 Km</Option>
            <Option value="10km">10 Km</Option>
            <Option value="5km">5 Km</Option>
            <Option value="Global">Global</Option>
          </Select>
        </Form.Item>
      </Form>
    );
  }
}

export default Filter;
