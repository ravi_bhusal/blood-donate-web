import React from "react";
import { Card, Icon, Switch } from "antd";
import { connect } from "react-redux";
import * as actions from "../../store/actions/user";
import Geocode from "react-geocode";

class ProfileBox extends React.Component {
  state = {};

  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  componentDidMount() {}

  componentWillReceiveProps(newProps) {
    this.setState({
      ...newProps
    });

    Geocode.setApiKey("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    console.log(newProps.currentLatitude, newProps.currentLongitude);
    if (
      newProps.currentLatitude !== null ||
      newProps.currentLongitude !== null
    ) {
      Geocode.fromLatLng(
        newProps.currentLatitude,
        newProps.currentLongitude
      ).then(
        response => {
          const address = response.results[0].formatted_address;
          console.log(address);
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  handleSwitchChange = checked => {
    console.log(`switch to ${checked}`, this.state.token);

    const postObj = {
      availability: checked
    };

    this.props.setAvailable(this.state.token, postObj);
  };
  render() {
    return (
      <Card style={{ width: 300 }} actions={[<Icon type="edit" />]}>
        <div>
          <img
            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
            height="40"
            width="40"
            alt="hello"
          />
          <h3 style={{ display: "inline", marginLeft: "10px" }}>Ravi Bhusal</h3>

          <p style={{ marginLeft: "50px", float: "left" }}>
            Available to donate?
          </p>
          <Switch
            name="availableSwitch"
            style={{
              display: "inline",
              marginLeft: "10px",
              float: "right"
            }}
            onChange={this.handleSwitchChange}
            checked={this.props.availability}
          />
        </div>
        <br />
        <br />
        <Icon type="fire" />{" "}
        <p style={{ display: "inline", marginLeft: "30px" }}>
          {`${this.props.bloodType}`.toUpperCase()}
        </p>
        <br />
        <br />
        <Icon type="phone" />{" "}
        <p style={{ display: "inline", marginLeft: "30px" }}>
          {this.state.phoneNumber}
        </p>
        <br />
        <br />
        <Icon type="environment" />{" "}
        <p style={{ display: "inline", marginLeft: "30px" }} />
        <br />
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,

    userId: state.user.userId,
    username: state.user.username,
    bloodType: state.user.blood_type,
    phoneNumber: state.user.phone_number,
    displayNumber: state.user.display_number,
    profileImage: state.user.profileImage,
    availability: state.user.availability,

    currentLatitude: state.currentLocation.currentLatitude,
    currentLongitude: state.currentLocation.currentLongitude,
    accuracy: state.currentLocation.accuracy
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setAvailable: (token, postObj) =>
      dispatch(actions.setAvailable(token, postObj))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileBox);
