import React from "react";

import { Layout, Menu, Col } from "antd";

import { connect } from "react-redux";
import * as actions from "../../store/actions/auth";

const { Header, Content } = Layout;

class ProfileBar extends React.Component {
  state = {};
  render() {
    return (
      <Layout className="layout" style={{}}>
        <Header
          style={{
            backgroundColor: "#fff",
            height: "48px",
            paddingLeft: "0px"
          }}
        >
          <Col span={2} />
          <Col span={20}>
            <Menu
              theme="light"
              mode="horizontal"
              defaultSelectedKeys={["2"]}
              style={{ lineHeight: "48px" }}
            >
              <Menu.Item key="1">Home</Menu.Item>
              <Menu.Item key="2">Notification</Menu.Item>
              <Menu.Item key="3">Message</Menu.Item>

              {this.props.isAuthenticated ? (
                <Menu.Item
                  style={{ float: "right" }}
                  key="5"
                  onClick={this.props.logout}
                >
                  Logout
                </Menu.Item>
              ) : (
                ""
              )}
              {this.props.isAuthenticated ? (
                <Menu.Item
                  style={{ float: "right", display: "inline" }}
                  key="4"
                >
                  Profile
                </Menu.Item>
              ) : (
                ""
              )}
            </Menu>{" "}
            />
          </Col>
          <Col span={2} />
        </Header>
        <Content style={{ padding: "0 50px" }} />
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(actions.logout())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ProfileBar);
