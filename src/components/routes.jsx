import React from "react";

import Login from "./login";
import SignUp from "./signup";
import HomePage from "./home";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

class BaseRouter extends React.Component {
  state = {};
  render() {
    return (
      <div>
        <PublicRoute
          exact
          path="/login"
          component={Login}
          isAuthenticated={this.props.isAuthenticated}
        />
        <PublicRoute
          exact
          path="/signup"
          component={SignUp}
          isAuthenticated={this.props.isAuthenticated}
        />
        <PrivateRoute
          exact
          path="/"
          component={HomePage}
          isAuthenticated={this.props.isAuthenticated}
          {...this.props}
        />
      </div>
    );
  }
}

export default BaseRouter;
