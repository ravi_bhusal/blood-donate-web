import React from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/currentLocation";

class Location extends React.Component {
  componentDidMount() {
    this.props.getCurrentLocation();
  }

  render() {
    return <div />;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getCurrentLocation: () => dispatch(actions.getCurrentLocation())
  };
};
export default connect(
  null,
  mapDispatchToProps
)(Location);
