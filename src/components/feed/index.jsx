import React from "react";
import { connect } from "react-redux";

import Status from "../status/";
import Filter from "../filter";

import { Row } from "antd";

import * as actions from "../../store/actions/userFeed";

class StatusList extends React.Component {
  state = {
    statusType: "available",
    bloodType: "a+",
    radius: "30km",
    numberOfScrolls: 0
  };

  componentDidMount() {}
  componentWillMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      if (nextProps.token !== undefined && nextProps.token !== null) {
        this.props.getFeedUsersList(
          nextProps.token,
          this.state.statusType,
          this.state.bloodType,
          this.state.radius,
          this.state.numberOfScrolls
        );
      }
    }
    //this.setState(nextProps);
  }

  handleFilterChange = (statusType, bloodType, radius) => {
    this.setState({
      statusType: statusType,
      bloodType: bloodType,
      radius: radius
    });
    if (this.props.token !== undefined && this.props.token !== null) {
      this.props.getFeedUsersList(
        this.props.token,
        statusType,
        bloodType,
        radius,
        this.state.numberOfScrolls
      );
    }
  };
  render() {
    return (
      <div
        style={{
          height: "700px",
          width: "auto",
          backgroundColor: "#fff",
          marginTop: "5px"
        }}
      >
        <Row
          style={{
            borderBottom: "1px solid #e6ecf0"
          }}
        >
          <Filter onChange={this.handleFilterChange.bind(this)} />
        </Row>
        <Row>
          <Status data={this.props.statuses} /> <br />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    statuses: state.userFeed.feedStatuses,
    loading: state.userFeed.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFeedUsersList: (token, statusType, bloodType, radius, numberOfScrolls) =>
      dispatch(
        actions.getFeedUserList(
          token,
          statusType,
          bloodType,
          radius,
          numberOfScrolls
        )
      )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusList);
