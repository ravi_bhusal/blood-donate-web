import React from "react";
import { List, Avatar, Icon } from "antd";
import moment from "moment";

const IconText = ({ type, text }) => (
  <span>
    <Icon
      type={type}
      style={{
        marginRight: 8
      }}
    />
    {text}
  </span>
);

const Status = props => {
  return (
    <List
      itemLayout="vertical"
      size="large"
      dataSource={props.data}
      renderItem={item => (
        <List.Item
          key={item.title}
          actions={[
            <IconText type="star-o" text="156" />,
            <IconText type="like-o" text="156" />,
            <IconText type="message" text="2" />
          ]}
          extra={
            <img
              width={272}
              alt="logo"
              src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"
            />
          }
        >
          <List.Item.Meta
            avatar={<Avatar src={item.profile_image} />}
            user={item.username}
            title={<a href={`/articles/${item.id}`}> {item.username} </a>}
            description={moment(item.posted_time)
              .startOf("hour")
              .fromNow()}
          />
          Hello! I am available to donate
        </List.Item>
      )}
    />
  );
};

export default Status;
