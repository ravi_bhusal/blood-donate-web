import axios from "axios";
import * as actionTypes from "./actionTypes";
import { history } from "../../components/app/index";
import * as actions from "./user";

axios.defaults.baseURL = "http://127.0.0.1:8000";
export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = token => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("expirationDate");

  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const authLogin = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());

    axios
      .post(`/rest-auth/login/`, {
        username: username,
        password: password
      })
      .then(res => {
        const token = res.data.token;
        const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
        const user = {
          username: res.data.user.username,
          userId: res.data.user.id,
          blood_type: res.data.user.blood_type,
          phone_number: res.data.user.phone_number,
          availability: res.data.user.availability,
          latitude: res.data.user.latitude,
          longitude: res.data.user.longitude,
          display_number: res.data.user.display_number,
          profile_image: res.data.user.profile_image
        };

        localStorage.setItem("token", JSON.stringify(token));
        localStorage.setItem("expirationDate", JSON.stringify(expirationDate));
        dispatch(authSuccess(token));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
        dispatch(checkAuthTimeout(3600));
        history.push("/");
      })
      .catch(error => {
        dispatch(authFail(error));
      });
  };
};

export const authSignup = (username, email, password1, password2) => {
  return dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());
    //axios.defaults.baseURL = "http://127.0.0.1:8000";
    axios
      .post(`/rest-auth/registration/`, {
        username: username,
        email: email,
        password1: password1,
        password2: password2
      })
      .then(res => {
        const token = res.data.token;
        const expirationDate = new Date(new Date().getTime() + 3600 * 1000);

        const user = {
          username: res.data.user.username,
          userId: res.data.user.id,
          blood_type: res.data.user.blood_type,
          phone_number: res.data.user.phone_number,
          availability: res.data.user.availability,
          latitude: res.data.user.latitude,
          longitude: res.data.user.longitude,
          display_number: res.data.user.display_number,
          profile_image: res.data.user.profile_image
        };

        localStorage.setItem("token", JSON.stringify(token));
        localStorage.setItem("expirationDate", JSON.stringify(expirationDate));

        dispatch(authSuccess(token));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
        dispatch(checkAuthTimeout(3600));
        history.push("/");
      })
      .catch(error => {
        dispatch(authFail(error));
      });
  };
};

export const authCheckState = () => {
  return dispatch => {
    const token = JSON.parse(localStorage.getItem("token"));

    if (token === undefined || token === null) {
      dispatch(logout());
      console.log("null token");
    } else {
      const expirationDate = new Date(
        JSON.parse(localStorage.getItem("expirationDate"))
      );

      if (expirationDate <= new Date()) {
        dispatch(logout());
        console.log("expiration Date expired");
      } else {
        // axios.defaults.baseURL = "http://127.0.0.1:8000";
        axios.defaults.headers.common = { Authorization: `JWT ${token}` };
        axios
          .get(`/api/currentuser/`)
          .then(res => {
            const user = {
              username: res.data.username,
              userId: res.data.id,
              blood_type: res.data.blood_type,
              phone_number: res.data.phone_number,
              availability: res.data.availability,
              latitude: res.data.latitude,
              longitude: res.data.longitude,
              display_number: res.data.display_number,
              profile_image: res.data.profile_image
            };
            console.log(user);
            dispatch(authSuccess(token));
            dispatch(actions.getCurrentUserSuccess(user));

            dispatch(
              checkAuthTimeout(
                (expirationDate.getTime() - new Date().getTime()) / 1000
              )
            );
          })
          .catch(err => {
            console.log(err);
            dispatch(actions.getCurrentUserFail(err));
          });
      }
    }
  };
};
