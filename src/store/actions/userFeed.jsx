import axios from "axios";
import * as actionTypes from "./actionTypes";

axios.defaults.baseURL = "http://127.0.0.1:8000";

const getFeedUsersListStart = () => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_START
  };
};

const getFeedUsersListSuccess = feedStatuses => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_SUCCESS,
    feedStatuses
  };
};
const getFeedUsersListFail = error => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_SUCCESS,
    error
  };
};

export const getFeedUserList = (
  token,
  statusType,
  bloodType,
  radius,
  numberOfScrolls
) => {
  return async dispatch => {
    dispatch(getFeedUsersListStart());
    axios.defaults.headers.common = { Authorization: `JWT ${token}` };

    if (statusType === "inNeed") {
      try {
        const feedStatuses = await Promise.resolve(
          fetchInNeedStatuses(statusType, bloodType, radius, numberOfScrolls)
        );

        dispatch(getFeedUsersListSuccess(feedStatuses));
      } catch (error) {
        dispatch(getFeedUsersListFail(error));

        throw error;
      }
    } else if (statusType === "available") {
      console.log(token);
      try {
        const feedStatuses = await fetchAvailableStatuses(
          statusType,
          bloodType,
          radius,
          numberOfScrolls
        );

        dispatch(getFeedUsersListSuccess(feedStatuses));
      } catch (error) {
        dispatch(getFeedUsersListFail(error));

        //throw error;
      }
    }
  };
};

const fetchAvailableStatuses = async (
  statusType,
  bloodType,
  radius,
  numberOfScrolls
) => {
  const res = await axios.get("/api/availableusers/", {
    params: {}
  });
  console.log(res.data);
  return res.data;
};

const fetchInNeedStatuses = async (
  statusType,
  bloodType,
  radius,
  numberOfScrolls
) => {
  if (statusType === "inNeed") {
    const statusList = await statusListFetch(
      statusType,
      bloodType,
      radius,
      numberOfScrolls
    );

    const userIdList = Array.from(new Set(statusList.map(({ user }) => user)));

    const userMap = await userMapFetchByIdList(userIdList);

    const statuses = statusList.map(doc => {
      const userFetched = userMap[doc.user] || `USER NOT FOUND`;

      return {
        ...doc,
        userFetched
      };
    });
    return statuses;
  } else if (statusType === "available") {
    const statusList = await statusListFetch(
      statusType,
      bloodType,
      radius,
      numberOfScrolls
    );
    return statusList;
  }
};

const statusListFetch = async (
  statusType,
  bloodType,
  radius,
  numberOfScrolls
) => {
  if (statusType === "available") {
    try {
      const { data } = await axios.get("/api/availableusers/", {
        params: {}
      });

      return data;
    } catch (error) {
      throw error;
    }
  } else if (statusType === "inNeed") {
    try {
      const { data } = await axios.get(`/api/status/?bloodType=${bloodType}`, {
        params: {}
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  //return data;
};

const userMapFetchByIdList = async idList => {
  const userList = await Promise.all(idList.map(id => userFetch({ id })));

  return userList.reduce((acc, doc) => {
    if (doc) {
      acc[doc.id] = doc;
    }

    return acc;
  }, {});
};

const userFetch = async ({ id }) => {
  try {
    const { data } = await axios.get(`/api/user/${id}/`);

    return data;
  } catch (error) {
    throw error;
  }
};
